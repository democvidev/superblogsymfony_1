<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Comments;
use App\Entity\Posts;
use DateTime;

class CommentsUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $comment = new Comments();
        $datetime = new DateTime();
        $post = new Posts();

        $comment->setCommentContent('contenu')
                ->setRgpd(true)
                ->setEmailAuthor('test@mail.com')
                ->setCommentCreatedAt($datetime)
                ->setPost($post);

        $this->assertTrue($comment->getCommentContent() === 'contenu');
        $this->assertTrue($comment->getRgpd() === true);
        $this->assertTrue($comment->getEmailAuthor() === 'test@mail.com');
        $this->assertTrue($comment->getCommentCreatedAt() === $datetime);
        $this->assertTrue($comment->getPost() === $post);

    }

    public function testIsFalse()
    {
        $comment = new Comments();
        $datetime = new DateTime();
        $post = new Posts();

        $comment->setCommentContent('contenu')
                ->setRgpd(true)
                ->setEmailAuthor('test@mail.com')
                ->setCommentCreatedAt($datetime)
                ->setPost($post);

        $this->assertFalse($comment->getCommentContent() === 'false');
        $this->assertFalse($comment->getRgpd() === false);
        $this->assertFalse($comment->getEmailAuthor() === 'false@mail.com');
        $this->assertFalse($comment->getCommentCreatedAt() === new DateTime());
        $this->assertFalse($comment->getPost() === new Posts());
    }

    public function testIsEmpty()
    {
        $comment = new Comments();

        $this->assertEmpty($comment->getCommentContent());
        $this->assertEmpty($comment->getRgpd());
        $this->assertEmpty($comment->getEmailAuthor());
        $this->assertEmpty($comment->getCommentCreatedAt());
        $this->assertEmpty($comment->getPost());
    }
}
