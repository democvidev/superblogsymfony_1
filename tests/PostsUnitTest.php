<?php

namespace App\Tests;

use App\Entity\Categories;
use PHPUnit\Framework\TestCase;
use App\Entity\Posts;
use App\Entity\Users;
use DateTime;

class PostsUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $post = new Posts();
        $datetime = new DateTime();
        $categorie = new Categories();
        $user = new Users();

        $post->setPostTitle('titre')
             ->setPostSlug('slug')
             ->setPostContent('contenu')
             ->setPostImage('image')
             ->setPostCreatedAt($datetime)
             ->setPostUpdatedAt($datetime)
             ->setUser($user)
             ->addCategorie($categorie);

        
        $this->assertTrue($post->getPostTitle() === 'titre');
        $this->assertTrue($post->getPostSlug() === 'slug');
        $this->assertTrue($post->getPostContent() === 'contenu');
        $this->assertTrue($post->getPostImage() === 'image');
        $this->assertTrue($post->getPostCreatedAt() === $datetime);
        $this->assertTrue($post->getPostUpdatedAt() === $datetime);
        $this->assertTrue($post->getUser() === $user);
        $this->assertContains($categorie, $post->getCategorie());

    }

    public function testIsFalse()
    {
        $post = new Posts();
        $datetime = new DateTime();
        $categorie = new Categories();
        $user = new Users();

        $post->setPostTitle('titre')
             ->setPostSlug('slug')
             ->setPostContent('contenu')
             ->setPostImage('image')
             ->setPostCreatedAt($datetime)
             ->setPostUpdatedAt($datetime)
             ->setUser($user)
             ->addCategorie($categorie);

        
        $this->assertFalse($post->getPostTitle() === 'false');
        $this->assertFalse($post->getPostSlug() === 'false');
        $this->assertFalse($post->getPostContent() === 'false');
        $this->assertFalse($post->getPostImage() === 'false');
        $this->assertFalse($post->getPostCreatedAt() === new DateTime());
        $this->assertFalse($post->getPostUpdatedAt() === new DateTime());
        $this->assertFalse($post->getUser() === new Users());
        $this->assertNotContains(new Categories(), $post->getCategorie());
    }


    public function testIsEmpty()
    {
        $post = new Posts();

        $this->assertEmpty($post->getPostTitle());
        $this->assertEmpty($post->getPostSlug());
        $this->assertEmpty($post->getPostContent());
        $this->assertEmpty($post->getPostImage());
        $this->assertEmpty($post->getPostCreatedAt());
        $this->assertEmpty($post->getPostUpdatedAt());
        $this->assertEmpty($post->getUser());
        $this->assertEmpty($post->getCategorie());
    }
}
