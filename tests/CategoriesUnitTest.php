<?php

namespace App\Tests;

use App\Entity\Categories;
use App\Entity\Users;
use App\Entity\Posts;
use PHPUnit\Framework\TestCase;
use DateTime;

class CategoriesUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $categorie = new Categories();
        $user = new Users();
        $datetime = new DateTime();
        $post = new Posts();

        $categorie->setCategTitle('titre')
                  ->setCategDescription('description')
                  ->setCategSlug('slug')
                  ->setCategCreatedAt($datetime)
                  ->setCategUpdatedAt($datetime)
                  ->setUser($user)
                  ->addPost($post);
        
        $this->assertTrue($categorie->getCategTitle() === 'titre');
        $this->assertTrue($categorie->getCategDescription() === 'description');
        $this->assertTrue($categorie->getCategSlug() === 'slug');
        $this->assertTrue($categorie->getCategCreatedAt() === $datetime);
        $this->assertTrue($categorie->getCategUpdatedAt() === $datetime);
        $this->assertTrue($categorie->getUser() === $user);
        $this->assertContains($post, $categorie->getPosts());

    }

    public function testIsFalse()
    {
        $categorie = new Categories();
        $user = new Users();
        $datetime = new DateTime();
        $post = new Posts();

        $categorie->setCategTitle('titre')
                  ->setCategDescription('description')
                  ->setCategSlug('slug')
                  ->setCategCreatedAt($datetime)
                  ->setCategUpdatedAt($datetime)
                  ->setUser($user)
                  ->addPost($post);


        $this->assertFalse($categorie->getCategTitle() === 'false');
        $this->assertFalse($categorie->getCategDescription() === 'false');
        $this->assertFalse($categorie->getCategSlug() === 'false');
        $this->assertFalse($categorie->getCategCreatedAt() === new DateTime());
        $this->assertFalse($categorie->getCategUpdatedAt() === new DateTime());
        $this->assertFalse($categorie->getUser() === new Users());
        $this->assertNotContains(new Posts(), $categorie->getPosts());
    }

    public function testIsEmpty()
    {
        $categorie = new Categories();

        $this->assertEmpty($categorie->getCategTitle());
        $this->assertEmpty($categorie->getCategDescription());
        $this->assertEmpty($categorie->getCategSlug());
        $this->assertEmpty($categorie->getCategCreatedAt());
        $this->assertEmpty($categorie->getCategUpdatedAt());
        $this->assertEmpty($categorie->getUser()); 
        $this->assertEmpty($categorie->getPosts());
    }
}
