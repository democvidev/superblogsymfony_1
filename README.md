# Symfony Project

Symfony Project est un blog qui permet de publier des articles.

## Rquierements


* Docker
* Docker-compose


## Installation

```bash

docker-compose up -d
docker exec -ti php73 bash
composer install
```
## Check
```bash
symfony check:requirements
```

## Tests
```bash
php bin/phpunit --testdox
```

## Usage

Page d'accueil du projet http://127.0.0.1:8000/  

Page d'accueil phpmyadmin http://127.0.0.1:8080   (user : root , psw : root)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)