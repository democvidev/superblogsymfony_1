<?php

namespace App\Entity;

use App\Repository\CategoriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoriesRepository::class)
 */
class Categories
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $categTitle;

    /**
     * @ORM\Column(type="text")
     */
    private $categDescription;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $categSlug;

    /**
     * @ORM\Column(type="datetime")
     */
    private $categCreatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $categUpdatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="categories")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=Posts::class, mappedBy="categorie")
     */
    private $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategTitle(): ?string
    {
        return $this->categTitle;
    }

    public function setCategTitle(string $categTitle): self
    {
        $this->categTitle = $categTitle;

        return $this;
    }

    public function getCategDescription(): ?string
    {
        return $this->categDescription;
    }

    public function setCategDescription(string $categDescription): self
    {
        $this->categDescription = $categDescription;

        return $this;
    }

    public function getCategSlug(): ?string
    {
        return $this->categSlug;
    }

    public function setCategSlug(string $categSlug): self
    {
        $this->categSlug = $categSlug;

        return $this;
    }

    public function getCategCreatedAt(): ?\DateTimeInterface
    {
        return $this->categCreatedAt;
    }

    public function setCategCreatedAt(\DateTimeInterface $categCreatedAt): self
    {
        $this->categCreatedAt = $categCreatedAt;

        return $this;
    }

    public function getCategUpdatedAt(): ?\DateTimeInterface
    {
        return $this->categUpdatedAt;
    }

    public function setCategUpdatedAt(?\DateTimeInterface $categUpdatedAt): self
    {
        $this->categUpdatedAt = $categUpdatedAt;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Posts[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Posts $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->addCategorie($this);
        }

        return $this;
    }

    public function removePost(Posts $post): self
    {
        if ($this->posts->removeElement($post)) {
            $post->removeCategorie($this);
        }

        return $this;
    }
}
